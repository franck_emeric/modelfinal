import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { SharedRoutingModule } from './shared-routing.module';
import { CommonModule } from '@angular/common';
import { FooterComponent,HeaderComponent  } from './layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselComponent } from './layout/carousel/carousel.component';
import { BodyComponent } from './layout/body/body.component';
import { ProductsComponent } from './layout/products/products.component';
import { EditorComponent } from './layout/editor/editor.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ChatbotComponent } from './layout/chatbot/chatbot.component';
import { FormsModule } from '@angular/forms';
import { ColorCircleModule } from 'ngx-color/circle';
import { HttpClientModule } from '@angular/common/http';
import { ColorSketchModule } from 'ngx-color/sketch';
import { GestionfaceComponent } from './layout/gestionface/gestionface.component';
import { FormclothesComponent } from './layout/formclothes/formclothes.component';
import { FormdispsComponent } from './layout/formdisps/formdisps.component';
import { FormpackagesComponent } from './layout/formpackages/formpackages.component';
import { FormgadgetsComponent } from './layout/formgadgets/formgadgets.component';
import { FormprintedComponent } from './layout/formprinted/formprinted.component';
import { LoginComponent } from './layout/login/login.component';
import { ModelformComponent } from './layout/modelform/modelform.component';
@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    CarouselComponent,
    BodyComponent,
    ProductsComponent,
    EditorComponent,
    SidebarComponent,
    ChatbotComponent,
    GestionfaceComponent,
    FormclothesComponent,
    FormdispsComponent,
    FormpackagesComponent,
    FormgadgetsComponent,
    FormprintedComponent,
    LoginComponent,
    ModelformComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ColorCircleModule,
    ColorSketchModule,
    SharedRoutingModule

  ],
  exports:[
    FooterComponent,
    HeaderComponent,
    CommonModule,
    NgbModule,
    CarouselComponent,
    BodyComponent,
    ProductsComponent,
    EditorComponent,
    FormsModule,
    ColorCircleModule,
    HttpClientModule,
    LoginComponent,


  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
