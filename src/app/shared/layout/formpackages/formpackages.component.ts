import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-formpackages',
  templateUrl: './formpackages.component.html',
  styleUrls: ['./formpackages.component.css']
})
export class FormpackagesComponent implements OnInit {
  @Input() url:any
  @Input() categorie:any
  articlename:any
  price:any
  promoprice:any
  details:any
  quantity:any
  packages:any
  s1=false
  s2=false
  s3=false
  s4=false
  s5=false
  s6=false
  s7=false
 
  hideeditor=false
  hidepacks=true
  file:any
  viewimage:any
  size={
    t1:"15/20",
    t2:"20/25",
    t3:"25/30",
    t4:"28/35",
    t5:"35/45",
    t6:"45/50",
    t7:"50/55"
  }
  typ:any
  oui=false
  file2:any
  viewi:any
  types=["models","produits"]
  constructor() { }

  ngOnInit(): void {
    console.log(this.categorie)
  }
showoui(){
  this.oui=true
}
showno(){
  this.oui=false
}
Upload(event:any){
 this.file=event.target.files[0]
 const reader = new FileReader();
  reader.onload = () => {
  
   this.viewimage= reader.result;
   };
   
   reader.readAsDataURL(this.file);
    console.log(this.file)
  
  }
  Uploade(event:any){
    this.file2=event.target.files[0]
    const reader = new FileReader();
     reader.onload = () => {
     
      this.viewi= reader.result;
      };
      
      reader.readAsDataURL(this.file2);
       console.log(this.file)
     
     }
  showS1(){
   this.s1=true
   this.size.t1
  }
  showS2(){
    this.s2=true
    this.size.t2
   }
   showS3(){
    this.s3=true
    this.size.t3
   }
   showS4(){
    this.s4=true
    this.size.t4
   }
   showS5(){
    this.s5=true
    this.size.t5
   }
   showS6(){
    this.s6=true
    this.size.t6
   }
   showS7(){
    this.s7=true
    this.size.t7
   }
   changetype(event:any){
    if(event.target.value == "produits"){
      this.typ="produits"
    }
    if(event.target.value == "models"){
      this.typ="model"
    }
  }
  register(){
    
    this.hideeditor=true
    this.hidepacks=false
    this.packages={
   description:JSON.stringify({
    url:this.viewimage,
    url2:this.viewi,
    name:this.articlename,
    price:this.price,
    promo:this.promoprice,
    made_with:this.details,
    size:this.size,
    owner:76,
    category:this.categorie,
    qty:this.quantity,
    type:this.typ,
    
   })
 }
 console.log(JSON.parse(this.packages.description))
  }

}
