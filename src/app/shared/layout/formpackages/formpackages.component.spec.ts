import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormpackagesComponent } from './formpackages.component';

describe('FormpackagesComponent', () => {
  let component: FormpackagesComponent;
  let fixture: ComponentFixture<FormpackagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormpackagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormpackagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
