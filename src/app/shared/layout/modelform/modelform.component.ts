import { Component, EventEmitter, OnInit,Output } from '@angular/core';
import { HttpservicesService } from 'src/app/core';

@Component({
  selector: 'app-modelform',
  templateUrl: './modelform.component.html',
  styleUrls: ['./modelform.component.scss']
})
export class ModelformComponent implements OnInit {
 len:any=[]
 @Output() changeComponent=new EventEmitter<boolean>();
 themes:any
 imag:any
 file:any
 hide=true
 detail:any
 hideeditor=false
 hidebody=true
 theme=["Anniversaire","Mariage","Bathème","Voeux","Fin d'année",]
  constructor(private http:HttpservicesService) { }

  ngOnInit(): void {
  }

uploadimage(event:any){
  this.file=event.target.files[0]
  console.log(this.file)
  const reader = new FileReader()
  reader.onload=()=>{
    this.imag=reader.result
    this.AddDesign()
    this.hide=false
    
};
reader.readAsDataURL(this.file)
console.log(this.file.type)
 
 
}
AddDesign(){
  this.hideeditor=true
  this.hidebody=false
 this.detail={
   description:JSON.stringify({
     url:this.imag,
     theme:this.themes,
     type:"design"
   })
  }
 

}
ChangeComponent(value:boolean){
  this.changeComponent.emit(value)
  console.log(value)

}
onchange(event:any){
  if(event.target.value==this.theme[0]){
    this.themes=this.theme[0]
    console.log(this.themes)
  }
  if(event.target.value==this.theme[1]){
    this.themes=this.theme[1]
    console.log(this.themes)
  }
  if(event.target.value==this.theme[2]){
    this.themes=this.theme[2]
    console.log(this.themes)
  }
  if(event.target.value==this.theme[3]){
    this.themes=this.theme[3]
    console.log(this.themes)
  }
  if(event.target.value==this.theme[4]){
    this.themes=this.theme[4]
    console.log(this.themes)
  }
}
}
