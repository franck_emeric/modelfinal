import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-formgadgets',
  templateUrl: './formgadgets.component.html',
  styleUrls: ['./formgadgets.component.css']
})
export class FormgadgetsComponent implements OnInit {
  @Input() categorie:any
  oui = false;
  visible = false;
  products:any
  file:any
  viewimage:any
  viewi:any
  file1:any
  vieweditor = false;
  hidesavecloth = true;
  articlename:any
  price:any
  promoprice:any
  quantity:any
  details:any
  types = ["models", "produits"];
  typ: any;
  num_category: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.categorie)
  }

  View1() {
    this.visible = true;
    this.oui = !this.oui;
  }

  View2() {
    this.visible = false;
  }
  UploadImage(event: any) {
    this.file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.viewimage = reader.result;
    };

    reader.readAsDataURL(this.file);
    console.log(this.file);
  }

  Uploade(event: any) {
    this.file1 = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.viewi = reader.result;
    };

    reader.readAsDataURL(this.file1);
    console.log(this.file1);
  }

  changetype(event: any) {
    if (event.target.value == "produits") {
      this.typ = "produits";
    }
    if (event.target.value == "models") {
      this.typ = "model";
    }

    console.log(this.num_category);
  }

  Editmodels(event: any) {
    this.vieweditor = true;
    this.hidesavecloth = false;
    this.products = {
      description: JSON.stringify({
        url: this.viewimage,
        url2: this.viewi,
        name: this.articlename,
        made_with: this.details,
        price: this.price,
        qty: this.quantity,
        promo: this.promoprice,
        owner: 76,
        category: this.categorie,
        type: this.typ,
      }),
    };

    console.log(JSON.parse(this.products.description));
  }

}
