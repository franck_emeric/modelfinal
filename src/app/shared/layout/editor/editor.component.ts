import { Component, Input, OnInit } from '@angular/core';
import { ColorEvent } from 'ngx-color';
import Typed from 'typed.js';
import { fabric } from 'fabric';
import { EditorserviceService } from 'src/app/core';
import { HttpservicesService } from 'src/app/core';
import { EditService } from 'src/app/core/edit.service';

declare var require :any
var $ = require("jquery")
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
@Input() data:any;
current_page:any=1
width=400;
height=400
canvas:any;
iH=400;
iW=400
hastext=false;
canvaswrapper:any
text:any;
testingpicture:any
textalign=["left","center","justify","right"]
cpt=0;
state:any
price:any
promoprice:any
details:any
models: any
articlename:any
num_category:any
clipart:any
undo:any=[];
redo:any=[];
qty:any
faces:any=[]
productstate:any=[]
Design:any=[]
isdone=false
Isokey=false
cptr=0
category=['Vêtement','Affichage','Emballage','Imprimerie','Gadget']
fonts = [
  {name:"Flowerheart",url:"./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf"},
  {name:"HussarBold",url:"./assets/fonts/HussarBoldWebEdition-xq5O.otf"},
  {name:'Branda',url:"./assets/fonts/Branda-yolq.ttf"},
  {name:"MarginDemo",url:"./assets/fonts/MarginDemo-7B6ZE.otf"},
  {name:"Kahlil",url:"./assets/fonts/Kahlil-YzP9L.ttf"},
  {name:"FastHand",url:"./assets/fonts/FastHand-lgBMV.ttf"},
  {name:"BigfatScript",url:"./assets/fonts/BigfatScript-jE96G.ttf"},
  {name:"Atlane",url:"./assets/fonts/Atlane-PK3r7.otf"},
  {name:"HidayatullahDemo",url:"./assets/fonts/Bismillahscript-4ByyY.ttf"},
  {name:"Robus",url:"./assets/fonts/HidayatullahDemo-mLp39.ttf"},
  {name:"Backslash",url:"./assets/fonts/Robus-BWqOd.otf"},
  {name:"ChristmasStory",url:"./assets/fonts/ChristmasStory-3zXXy.ttf"},
  {name:"ConeriaScript_Slanted",url:"./assets/fonts/Demo_ConeriaScript_Slanted.ttf"},
  {name:"ConeriaScript",url:"./assets/fonts/Demo_ConeriaScript.ttf"},
  {name:"Sydney",url:"./assets/fonts/Sydney.ttf"},
  {name:"Sydney_Signature",url:"./assets/fonts/Sydney_Signature.ttf"},
  {name:"SweetValentines", url:"./assets/fonts/SweetValentinesDemo.ttf"},
  {name:"28DaysLater",url:"./assets/fonts/28-Days-Later.ttf"},
  {name:"Christian-Sunday",url:"./assets/fonts/Christian-Sunday-Italic.ttf"}

];

 	//id_model 	front 	back 	obj 	price 	category 	promo 	qty 	type 	added_at 	updated_at 	owner 	description
testcanvas:any;
colors=["blue","white","black","red","orange","yellow","green","#999999","#454545","#800080","#000080","#00FF00","#800000","#008080"]
  constructor(private Editor:EditorserviceService,private http:HttpservicesService, private Edit:EditService) { }

  ngOnInit(): void {
    console.log(this.data)

    this.http.getclipart(this.current_page).subscribe(res=>{
      this.clipart=res;
       this.clipart=this.clipart.cliparts;
  },
  er=>{console.log(er)})


  this.canvas= new fabric.Canvas('canvas-wrapper',{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false

  });
  this.canvas.filterBackend=new fabric.WebglFilterBackend();
  var url=JSON.parse(this.data.description).url
  var url2= this.data.url2
  this.faces.push(url,url2)
   this.Editor.MakeImage(url,this.canvas,false)
var canvasWrapper:any = document.getElementById('wrapper');
// initial dimensions
canvasWrapper.style.width = this.iW;
canvasWrapper.style.height = this.iH;

setInterval(() => {
  const newWidth = canvasWrapper.clientWidth
  const newHeight = canvasWrapper.clientHeight
  if (newWidth !== this.iW || newHeight !== this.iH) {
    this.width = newWidth
    this.height = newHeight
    this.iW=this.width;
    this.iH=this.height
    this.canvas.setWidth(newWidth)
    this.canvas.setHeight(newHeight)
    this.canvas.requestRenderAll()
  }
},100)
  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);
 // this.Editor.MakeImage(JSON.parse(this.data.description).url,this.canvas);
}

textfont(item:any){
  let data= item.target.value
  console.log(data.substr(0,data.indexOf("*")))
  this.Editor.textfont(Object.assign({},{name:data.substr(0,data.indexOf("*")),url:data.substr(data.indexOf("*")+1,data.length-1)}),this.canvas)
}
  makeItalic(){
    this.Editor.italic(this.canvas)
  }

  makeBold(){
    this.Editor.bold(this.canvas)
  }

  underlineText(){
    this.Editor.underline(this.canvas)
  }


  overlineText(){
    this.Editor.overline(this.canvas)
  }


  addText(){
    if(!this.hastext){
      this.Editor.addText(this.canvas);

      this.hastext=true;
    }
  }


  copy(){
    this.Editor.copy(this.canvas)
  }

  paste(){
    this.Editor.paste(this.canvas)
  }


  removeItem(){

    this.Editor.remove(this.canvas);
  }

  InputChange(Inputtext:any){
    if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){

      if(this.cpt==0){
        this.text=this.canvas.getActiveObject().text+" "+ this.text
        this.cpt=this.cpt+1
        this.canvas.getActiveObject().text= this.text
        this.canvas.requestRenderAll();
      }else{
        this.canvas.getActiveObject().text= this.text
        this.canvas.requestRenderAll();
      }

    }else{
      let text= new fabric.Textbox(this.text,{
        top:200,
        left:200,
        fill:"blue",
        fontSize:38,
        fontStyle:'normal',
        cornerStyle:'circle',
        selectable:true,
        borderScaleFactor:1,
        overline:false,
        lineHeight:1.5
      });

      this.canvas.add(text).setActiveObject(text);
      this.canvas.renderAll(text);
      this.canvas.requestRenderAll();
      this.canvas.centerObject(text);
    }


  }

  texteclor($event:ColorEvent){
    this.Editor.textcolor($event.color.hex,this.canvas);

  }



  setItem(event:any){
  this.Editor.setitem(event,this.canvas)
  }


  onFileUpload(event:any){
    let file = event.target.files[0];
    if(!this.Editor.handleChanges(file)){

      const reader = new FileReader();

    reader.onload = () => {
      let url:any = reader.result;
      fabric.Image.fromURL(url,(oImg) =>{
      oImg.set({
          scaleX:0.5,
          scaleY:0.5,
          crossOrigin: "Anonymous",
    });
      this.canvas.add(oImg).setActiveObject(oImg);
      this.canvas.centerObject(oImg);
      this.canvas.renderAll(oImg)

    })
      };
      reader.readAsDataURL(file);
    console.log(file)
    }

  }


  resize(){
    this.canvas.setWidth(this.width);
    this.canvas.setHeight(this.height);
    this.canvas.centeredScaling=true
     this.canvas.renderAll()

  }


  InputSize(){
    var canvasWrapper:any = document.getElementById('wrapper');
    canvasWrapper.style.width = this.width;
    canvasWrapper.style.height = this.height;
    this.canvas.setWidth(this.width);
    this.canvas.setHeight(this.width);
  }
// initial dimensions


 
  changeCategories(event:any){
    if(event.target.value == "Affichage"){
      this.num_category = 3
    }
    if(event.target.value == "Gadget"){
      this.num_category = 5
    }
    if(event.target.value == "Emballage"){
      this.num_category = 2
    }
    if(event.target.value == "Vêtement"){
      this.num_category = 1
    }
    if(event.target.value == "Imprimerie"){
      this.num_category = 4
    }
    console.log(this.num_category)
  }
  currentPage(event:any){
    var page=event.target.id
      if(+page){
        this.current_page=page
        
        this.http.getclipart(page).subscribe(res=>{
          let data:any=res;
          if(data.status==200){
            if(data.cliparts.length>0){
              
              this.clipart=data.cliparts
            }else{
             
            }
          
          }else{
           
          }
        }, )
      }
  
  }
  sendBack(){
    this.Edit.sendBack(this.canvas)
  }
  
  sendForward(){
    this.Edit.sendForward(this.canvas)
  }
  textAlign(val:any){
    this.Edit.textAlign(this.canvas,val)
  
  }
  
  Redo(){
    $('#redo').click((e:any)=> {
    })
  }
  Undo(){
    $('#undo').click((e:any)=> {
    });
   
  }



register(){
  console.log(this.data.description.type)
  this.data.description=JSON.parse(this.data.description)
 if(this.data.description.type=="design"){
   this.ajout()
 }
  if(this.data.description.type=="produits" || this.data.description.type=="model"){
   this.SaveModel()
 }


}

SaveModel(){
  var data:any={}

  if(this.faces[1]==null){
  var json = this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
  this.productstate.push(JSON.stringify(json))
  this.faces[0]=this.canvas.toDataURL()
  var gil=this.data.description
  Object.assign(data, {
   description:JSON.stringify({
     name:gil.name,
     made_with:gil.made_with,
     price:gil.price,
     promo:gil.promo,
     owner:gil.owner,
     qty:gil.qty,
     size:gil.size,
     material:gil.material,
     category:gil.category,
     type:gil.type, 
     
   }),
   obj:this.productstate[0],
   objf:JSON.stringify(null),
   width:this.width,
   height:this.height

  })
  
  this.http.add(data).subscribe(
    res=>{
      console.log(res)
      if(res.insertId){
        alert("bien")
      }
    },
    err=>{
      console.log(err)
    }
  )

  }

  if(this.cptr==0 && this.faces[1]){
    var json1= this.canvas.toJSON(['lockMovementX','lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.productstate.push(JSON.stringify(json1))
    this.faces[0]=this.canvas.toDataURL()
    this.canvas.clear()
    this.Editor.MakeImage(this.faces[1],this.canvas,false)
    this.cptr=this.cptr +1
  }
 
  if(this.cptr>1){
    var json2= this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
    this.productstate.push(JSON.stringify(json2))
    this.faces[1]=this.canvas.toDataURL()
   var gil=this.data.description
   Object.assign(data, {
    description:JSON.stringify({
      name:gil.name,
      made_with:gil.made_with,
      price:gil.price,
      promo:gil.promo,
      owner:gil.owner,
      qty:gil.qty,
      size:gil.size,
      material:gil.material,
      category:gil.category,
      type:gil.type, 
      
    }),
    obj:this.productstate[0],
    objf:this.productstate[1],
    width:this.width,
    height:this.height

   })
   this.http.add(data).subscribe(
    res=>{
      console.log(res)
      if(res.insertId){
        alert("bien")
      }
    },
    err=>{
      console.log(err)
    }
  )
  
  }else{
        this.cptr=this.cptr +1

  }

}

Spinner(){
  this.Isokey=!this.Isokey
}
ajout(){
    var table:any=[]
  for(let item of this.canvas.getObjects()){
    table.push(item.toJSON())
  
  }
  this.http.Create_design(table).subscribe(
    res=>{
      console.log(res)
    
  }, err=>{
    console.log(err)
  })
  console.log(table)
  
}

}
