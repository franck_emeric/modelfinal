import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormclothesComponent } from './formclothes.component';

describe('FormclothesComponent', () => {
  let component: FormclothesComponent;
  let fixture: ComponentFixture<FormclothesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormclothesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormclothesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
