import { Component, OnInit } from '@angular/core';
import Typed from 'typed.js';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 cat:any
 
 hidecloth=false
 hidepacks=false
 hideprint=false
 hidegadget=false
 hidedisp=false
 hidedesign:Boolean=false
hideody=true
  constructor() { }

  ngOnInit(): void {
    //let typed= new Typed ('#typed',{
    //  strings:['Bienvenu sur ton tableau de bord', 'Bon boulot a toi!!'],
    //  typeSpeed:140,
    //  backSpeed:140,
    //  loop:true,
    //  smartBackspace:false,
    //  showCursor:false,
   //   
    //  
    //});
  }
  showdesig(){
    this.hidedesign=true
    this.hideody=false
    this.hidecloth=false
    this.hidepacks=false
    this.hideprint=false
    this.hidegadget=false
   this.hidedisp=false
  }
  showbody(){
    this.hideody=true
    this.hidedesign=false
    this.hidecloth=false
    this.hidepacks=false
    this.hideprint=false
    this.hidegadget=false
   this.hidedisp=false
  }
showcloth(){
  this.hideody=false
  this.hidedesign=false
  this.hidecloth=true
  this.hidepacks=false
  this.hideprint=false
  this.hidegadget=false
 this.hidedisp=false

  this.cat=1
}
showpack(){
  this.hidedesign=false
  this.hideody=false
  this.hidecloth=false
  this.hidepacks=true
  this.hideprint=false
  this.hidegadget=false
  this.hidedisp=false
  this.cat=2
}
showprint(){
  this.hidedesign=false
  this.hideody=false
  this.hidecloth=false
  this.hidepacks=false
  this.hideprint=true
  this.hidegadget=false
  this.hidedisp=false
  this.cat=4
}
showdisp(){
  this.hideody=false
  this.hidedesign=false
  this.hidecloth=false
  this.hidepacks=false
  this.hideprint=false
  this.hidegadget=false
  this.hidedisp=true
  this.cat=3
}
showgadget(){
  this.hideody=false
  this.hidedesign=false
  this.hidecloth=false
  this.hidepacks=false
  this.hideprint=false
  this.hidegadget=true
  this.hidedisp=false
  this.cat=5
}

}
